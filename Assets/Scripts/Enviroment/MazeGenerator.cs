﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MazeGenerator : MonoBehaviour {

	public SpriteRenderer mazeBlockPrefab;
	public MazeGenerator generatorPrefab;
	public Vector2 gridSize = new Vector2(16, 16);

	public bool generateOnSpawned = false;

	public Vector2 GetWorldCoords(Vector2 gridCoords)
	{
		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;
		
		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * mazeBlockPrefab.transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * mazeBlockPrefab.transform.localScale.y;

		Vector3 scaled = gridCoords;
		scaled.x *= spacing.x;
		scaled.y *= spacing.y;

		return transform.position + scaled;
	}

	public Vector2 GetEmptyLocation()
	{
		return GetWorldCoords(clearBlocks[Random.Range(0, clearBlocks.Count)]);
	}

	public Vector2 GetDeadEnd()
	{
		return GetWorldCoords(deadEnds[Random.Range(0, deadEnds.Count)]);
	}

	void Awake()
	{
		if(generateOnSpawned)
			GenerateMap();
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.gameObject.layer != LayerMask.NameToLayer("Player"))
			return;

		if(topMaze == null)
		{
			topMaze = GenerateTopMap();
			topMaze.bottomExit = new Vector2(topExit.x, 0);
			topMaze.bottomMaze = this;
			topMaze.GenerateMap();
		}

		if(rightMaze == null)
		{
			rightMaze = GenerateRightMap();
			rightMaze.leftExit = new Vector2(0, rightExit.y);
			rightMaze.leftMaze = this;
			rightMaze.GenerateMap();
		}

		if(bottomMaze == null)
		{
			bottomMaze = GenerateBottomMap();
			bottomMaze.topExit = new Vector2(bottomExit.x, bottomMaze.gridSize.y - 1);
			bottomMaze.topMaze = this;
			bottomMaze.GenerateMap();
		}

		if(leftMaze == null)
		{
			leftMaze = GenerateLeftMap();
			leftMaze.rightExit = new Vector2(leftMaze.gridSize.x - 1, leftExit.y);
			leftMaze.rightMaze = this;
			leftMaze.GenerateMap();
		}

		if(topMaze.rightMaze == null)
		{
			MazeGenerator topRight = topMaze.GenerateRightMap();
			topMaze.rightMaze = topRight;
			rightMaze.topMaze = topRight;
			topRight.leftMaze = topMaze;
			topRight.bottomMaze = rightMaze;
			topRight.leftExit = new Vector2(0, topMaze.rightExit.y);
			topRight.bottomExit = new Vector2(rightMaze.topExit.x, 0);
			topRight.GenerateMap();
		}

		if(bottomMaze.rightMaze == null)
		{
			MazeGenerator bottomRight = bottomMaze.GenerateRightMap();
			bottomMaze.rightMaze = bottomRight;
			rightMaze.bottomMaze = bottomRight;
			bottomRight.leftMaze = bottomMaze;
			bottomRight.topMaze = rightMaze;
			bottomRight.leftExit = new Vector2(0, bottomMaze.rightExit.y);
			bottomRight.topExit = new Vector2(rightMaze.bottomExit.x, bottomRight.gridSize.y - 1);
			bottomRight.GenerateMap();
		}

		if(bottomMaze.leftMaze == null)
		{
			MazeGenerator bottomLeft = bottomMaze.GenerateLeftMap();
			bottomMaze.leftMaze = bottomLeft;
			leftMaze.bottomMaze = bottomLeft;
			bottomLeft.rightMaze = bottomMaze;
			bottomLeft.topMaze = leftMaze;
			bottomLeft.rightExit = new Vector2(bottomLeft.gridSize.x - 1, bottomMaze.leftExit.y);
			bottomLeft.topExit = new Vector2(leftMaze.bottomExit.x, bottomLeft.gridSize.y - 1);
			bottomLeft.GenerateMap();
		}

		if(topMaze.leftMaze == null)
		{
			MazeGenerator topLeft = topMaze.GenerateLeftMap();
			topMaze.leftMaze = leftMaze;
			leftMaze.topMaze = topLeft;
			topLeft.rightMaze = topMaze;
			topLeft.bottomMaze = leftMaze;
			topLeft.rightExit = new Vector2(topLeft.gridSize.x - 1, topMaze.leftExit.y);
			topLeft.bottomExit = new Vector2(leftMaze.topExit.x, 0);
			topLeft.GenerateMap();
		}
	}

	private MazeGenerator GenerateTopMap()
	{
		if(topMaze != null)
			return topMaze;

		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;
		
		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * mazeBlockPrefab.transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * mazeBlockPrefab.transform.localScale.y;
		
		Vector3 scaled = gridSize;
		scaled.x *= 0;
		scaled.y *= spacing.y;
		
		GameObject nextGrid = (Instantiate(generatorPrefab) as MazeGenerator).gameObject;
		nextGrid.transform.position = transform.position + (Vector3)(scaled);

		return nextGrid.GetComponent<MazeGenerator>();
	}

	private MazeGenerator GenerateRightMap()
	{
		if(rightMaze != null)
			return rightMaze;

		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;
		
		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * mazeBlockPrefab.transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * mazeBlockPrefab.transform.localScale.y;
		
		Vector3 scaled = gridSize;
		scaled.x *= spacing.x;
		scaled.y *= 0;
		
		GameObject nextGrid = (Instantiate(generatorPrefab) as MazeGenerator).gameObject;
		nextGrid.transform.position = transform.position + (Vector3)(scaled);

		return nextGrid.GetComponent<MazeGenerator>();
	}

	private MazeGenerator GenerateBottomMap()
	{
		if(bottomMaze != null)
			return bottomMaze;

		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;
		
		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * mazeBlockPrefab.transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * mazeBlockPrefab.transform.localScale.y;
		
		Vector3 scaled = gridSize;
		scaled.x *= 0;
		scaled.y *= -spacing.y;
		
		GameObject nextGrid = (Instantiate(generatorPrefab) as MazeGenerator).gameObject;
		nextGrid.transform.position = transform.position + (Vector3)(scaled);
		
		return nextGrid.GetComponent<MazeGenerator>();
	}

	private MazeGenerator GenerateLeftMap()
	{
		if(leftMaze != null)
			return leftMaze;

		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;
		
		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * mazeBlockPrefab.transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * mazeBlockPrefab.transform.localScale.y;
		
		Vector3 scaled = gridSize;
		scaled.x *= -spacing.x;
		scaled.y *= 0;
		
		GameObject nextGrid = (Instantiate(generatorPrefab) as MazeGenerator).gameObject;
		nextGrid.transform.position = transform.position + (Vector3)(scaled);
		
		return nextGrid.GetComponent<MazeGenerator>();
	}

	private void GenerateMap()
	{
		if(grid != null)
			return;

		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;
		
		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * mazeBlockPrefab.transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * mazeBlockPrefab.transform.localScale.y;

		Vector2 scaled = gridSize - Vector2.one;
		scaled.x *= spacing.x;
		scaled.y *= spacing.y;

		BoxCollider2D spawnTrigger = gameObject.AddComponent<BoxCollider2D>();
		spawnTrigger.offset = (Vector3)(scaled / 2f);
		spawnTrigger.size = scaled;
		spawnTrigger.isTrigger = true;

		grid = GenerateGrid(transform.position);
		GenerateMaze(grid);

		gameObject.BroadcastMessage("OnMapGenerated", this, SendMessageOptions.DontRequireReceiver);
	}

	private GameObject[] GenerateGrid(Vector2 gridLocation)
	{
		GameObject grid = new GameObject("grid");
		grid.transform.position = new Vector3(gridLocation.x, gridLocation.y, 0);
		grid.transform.parent = gameObject.transform;

		Sprite mazeBlockSprite = mazeBlockPrefab.sprite;

		Vector2 spacing = Vector2.zero;
		spacing.x = mazeBlockSprite.bounds.size.x * transform.localScale.x;
		spacing.y = mazeBlockSprite.bounds.size.y * transform.localScale.y;

		GameObject[] gridObjs = new GameObject[(int)gridSize.x * (int)gridSize.y];

		for(float x = gridLocation.x, xi = 0; xi < gridSize.x; x += spacing.x, xi++)
		{
			for(float y = gridLocation.y, yi = 0; yi < gridSize.y; y += spacing.y, yi++)
			{
				gridObjs[(int)gridSize.x * (int)xi + (int)yi] = (Instantiate(mazeBlockPrefab, new Vector3(x, y, 0), Quaternion.identity) as SpriteRenderer).gameObject;
				//gridObjs[(int)gridSize.y * (int)yi + (int)xi].transform.parent = grid.transform;
			}
		}

		return gridObjs;
	}

	private int GetCoordIndex(Vector2 gridLocation)
	{
		return (int)gridSize.y * (int)gridLocation.y + (int)gridLocation.x;
	}

	private void GenerateMaze(GameObject[] grid)
	{
		Vector2 startPoint;
		if(bottomExit == Vector2.zero)
			startPoint = new Vector2((int)Random.Range(1f, gridSize.x - 2f), 0);
		else
			startPoint = bottomExit;
		bottomExit = startPoint;

		Traverse(startPoint, grid);

		while(topExit == Vector2.zero)
		{
			Vector2 point = new Vector2((int)Random.Range(1f, gridSize.x - 2f), gridSize.y - 2f);
			if(IsBlockClear(point, grid))
			{
				Vector2 exitPoint = GetAdjacentPoint(point, GridDirection.UP);
				ClearBlock(exitPoint, grid);
				topExit = exitPoint;
				break;
			}
		}

		while(rightExit == Vector2.zero)
		{
			Vector2 point = new Vector2(gridSize.x - 2f, (int)Random.Range(1f, gridSize.y - 2f));
			if(IsBlockClear(point, grid))
			{
				Vector2 exitPoint = GetAdjacentPoint(point, GridDirection.RIGHT);
				ClearBlock(exitPoint, grid);
				rightExit = exitPoint;
				break;
			}
		}

		while(leftExit == Vector2.zero)
		{
			Vector2 point = new Vector2(1, (int)Random.Range(1f, gridSize.y - 2f));
			if(IsBlockClear(point, grid))
			{
				Vector2 exitPoint = GetAdjacentPoint(point, GridDirection.LEFT);
				ClearBlock(exitPoint, grid);
				leftExit = exitPoint;
				break;
			}
		}

		if(IsBlockClear(GetAdjacentPoint(bottomExit, GridDirection.UP), grid))
			Traverse(bottomExit, grid);
		if(IsBlockClear(GetAdjacentPoint(topExit, GridDirection.DOWN), grid))
			Traverse(topExit, grid);
		if(IsBlockClear(GetAdjacentPoint(leftExit, GridDirection.RIGHT), grid))
			Traverse(leftExit, grid);
		if(IsBlockClear(GetAdjacentPoint(rightExit, GridDirection.LEFT), grid))
			Traverse(rightExit, grid);
	}

	private void Traverse(Vector2 startPoint, GameObject[] grid)
	{
		ClearBlock(startPoint, grid);

		int[] directions = {0, 1, 2, 3};

		bool deadEnd = true;
		int checkCount = 0;
		while(checkCount < 4)
		{
			int checkDirection = directions[Random.Range(0, 4)];

			if(checkDirection == -1)
				continue;

			if(CanTransverse(GetAdjacentPoint(startPoint, (GridDirection)checkDirection), grid))
			{
				Traverse(GetAdjacentPoint(startPoint, (GridDirection)checkDirection), grid);
				deadEnd = false;
			}

			checkCount++;
			directions[checkDirection] = -1;
		}

		if(deadEnd)
			deadEnds.Add(startPoint);
	}

	private bool CanTransverse(Vector2 point, GameObject[] grid)
	{
		if(IsBlockClear(point, grid))
			return false;

		if(IsEdgeBlock(point, grid))
			return false;

		int adjacentBlocks = 0;
		for(int i = 0; i < 4; i++)
		{
			if(!IsBlockClear(GetAdjacentPoint(point, (GridDirection)i), grid))
				adjacentBlocks++;
		}

		return adjacentBlocks >= 3;
	}

	private bool IsEdgeBlock(Vector2 point, GameObject[] grid)
	{
		return (point.x < 0 || point.x >= gridSize.x) || (point.y < 0 || point.y >= gridSize.y);
	}

	private bool IsBlockClear(Vector2 point, GameObject[] grid)
	{
		if((point.x < 0 || point.x >= gridSize.x) || (point.y < 0 || point.y >= gridSize.y))
		   return true;

		GameObject block = grid[(int)gridSize.y * (int)point.x + (int)point.y];
		return block == null;
	}

	private void ClearBlock(Vector2 point, GameObject[] grid)
	{
		int gridIndex =(int)gridSize.y * (int)point.x + (int)point.y;
		GameObject block = grid[gridIndex];

		if(block == null)
			return;

		DestroyObject(block);

		grid[gridIndex] = null;
		clearBlocks.Add(point);
	}

	private Vector2 GetAdjacentPoint(Vector2 point, GridDirection direction)
	{
		switch(direction)
		{
		case GridDirection.UP:
			return point + Vector2.up;
		case GridDirection.LEFT:
			return point - Vector2.right;
		case GridDirection.RIGHT:
			return point + Vector2.right;
		case GridDirection.DOWN:
			return point - Vector2.up;
		}

		return point;
	}

	private Vector2 DirectionToVector(GridDirection direction)
	{
		switch(direction)
		{
		case GridDirection.UP:
			return  Vector2.up;
		case GridDirection.LEFT:
			return -Vector2.right;
		case GridDirection.RIGHT:
			return Vector2.right;
		case GridDirection.DOWN:
			return -Vector2.up;
		}
		
		return Vector2.zero;
	}

	private enum GridDirection
	{
		UP, RIGHT, DOWN, LEFT
	}

	private List<Vector2> deadEnds = new List<Vector2>();
	private List<Vector2> clearBlocks = new List<Vector2>();
	private Vector2 topExit, bottomExit, leftExit, rightExit;
	private MazeGenerator topMaze, rightMaze, bottomMaze, leftMaze;

	private GameObject[] grid;
}
