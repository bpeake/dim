﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapItemSpawner : MonoBehaviour
{
	public GameObject itemPrefab;
	public int maxItemCount = 3;
	public int minItemCount = 1;
	public float difficultyRatio = 3f;

	void OnMapGenerated(MazeGenerator generator)
	{
		DifficultyController difficulty = GameObject.FindObjectOfType<DifficultyController>();

		int itemCount = (int)Mathf.Lerp(maxItemCount, minItemCount, difficulty.Difficulty / difficultyRatio);



		for(int i = 0; i < itemCount; i++)
		{
			Vector2 deadEnd = generator.GetDeadEnd();

			bool hasSpawned = false;
			foreach(Vector2 point in itemSpawnLocations)
			{
				if((deadEnd - point).sqrMagnitude < 0.01)
				{
					hasSpawned = true;
					break;
				}
			}

			if(hasSpawned)
			{
				i--;
				continue;
			}

			Instantiate(itemPrefab, deadEnd, Quaternion.identity);
		}
	}
	private List<Vector2> itemSpawnLocations = new List<Vector2>();
}
