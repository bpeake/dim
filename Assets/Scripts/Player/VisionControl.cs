﻿using UnityEngine;
using System.Collections;

public class VisionControl : MonoBehaviour
{
	public GameStats gameStats;
	public SpriteRenderer vision;
	
	// Update is called once per frame
	void Update ()
	{
		Color color = vision.color;
		color.a = Mathf.Lerp(1, 0, (gameStats.Life / 100f) * (gameStats.Life / 100f));

		vision.color = color;
	}
}
