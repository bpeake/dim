﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour {

	public float speed = 5;

	// Use this for initialization
	void Start ()
	{
		physics = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector2 movementDirection = Vector2.zero;
		movementDirection.x = Input.GetAxisRaw("Horizontal");
		movementDirection.y = Input.GetAxisRaw("Vertical");

		movementDirection = movementDirection.normalized * (Mathf.Clamp01(movementDirection.magnitude) * speed);

		physics.velocity = movementDirection;
	}

	private Rigidbody2D physics;
}
