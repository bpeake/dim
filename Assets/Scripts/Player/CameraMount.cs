﻿using UnityEngine;
using System.Collections;

public class CameraMount : MonoBehaviour
{
	public Transform mountTarget;

	public Vector3 offset = new Vector3(0, 0, -10);

	void LateUpdate()
	{
		transform.position = mountTarget.position + offset;
	}
}
