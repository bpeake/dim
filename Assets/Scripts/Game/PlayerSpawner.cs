﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

	public MazeGenerator maze;
	public PlayerMovement player;

	// Use this for initialization
	void Start ()
	{
		player.transform.position = maze.GetEmptyLocation();
	}
}
