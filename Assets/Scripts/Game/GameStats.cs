﻿using UnityEngine;
using System.Collections;

public class GameStats : MonoBehaviour 
{
	public DifficultyController difficulty;

	public float Life
	{
		get{return life;}
		set{life = value;}
	}

	public float LifeRate
	{
		get{return lifeRate;}
		set{lifeRate = value;}
	}

	void Update()
	{
		life -= Time.deltaTime * lifeRate * (difficulty.Difficulty + 1f);

		if(life <= 0)
			life = 0;
	}

	private float life = 100;
	private float lifeRate = 1;
}
