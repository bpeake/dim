﻿using UnityEngine;
using System.Collections;

public class FlamePickup : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other)
	{
		GameObject.FindObjectOfType<GameStats>().Life = 100;
		Destroy(gameObject);
	}
}
