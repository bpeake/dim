﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideCursorOnStartup : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        Cursor.visible = false;
    }
}