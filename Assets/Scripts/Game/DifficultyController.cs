﻿using UnityEngine;
using System.Collections;

public class DifficultyController : MonoBehaviour
{
	public float difficultyPolynomial = 2;
	public float difficultyRate = 0.01f;

	public float Difficulty
	{
		get{return Mathf.Pow(linearDifficulty, difficultyPolynomial);}
	}

	// Update is called once per frame
	void Update ()
	{
		linearDifficulty += difficultyRate * Time.deltaTime;
	}

	private float linearDifficulty = 0;
}
